from bottle import route, run, static_file

@route('/')
def index():
    user_name_from_db = "Hovak"
    return '<html><body bgcolor="#ddddff"><h1 style="font-size:72">YO! ' + user_name_from_db + '</h1><a href="index.html">click here!</a></body></html>'

# my comment blah blah

@route('/<filename>')
def serve_folder(filename):
    return static_file(filename, root='./public/')


run()
