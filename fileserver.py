#bottle_fileserve2.py
#
#Author: bsod64@gmail.com
#
#Note: Please inform me of any problems with the code that I have overlooked :D
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.


from bottle import route, run, static_file
import sys
import os
import glob
import bottle


app = bottle.Bottle() #A new application
serve_path = os.getcwd()  #get the directory in which the script was started.
colorinfo = ['<tr style = "background-color:skyblue">','<tr>'] #CSS to color alternate rows

def constructPath(path):
        '''Convert path to windows path format.'''
        if(sys.platform=='win32'):
            return "\\"+path.replace('/','\\')
        return path #Return same path if on linux or unix

show_hidden = False #show hidden files?
#set show_hidden to True if hidden files should be listed.


class DirLister:
    '''Instantiate this class to serve a directory listing.

    The listing begins in the current directory.
    NOTE: The directory listing does not show this file.
    '''
    def __init__(self, hostname='127.0.0.1', hostport=8080, showhidden=False,):
        '''Initialize. Can be used to pass hostname and port.'''
        run(app, host=hostname,port=hostport) #Run the application using Bottle


    @app.route('/<filename:re:.*>') #match any string after /
    def serve(filename):
        i=1  #To alternate background color
        path = serve_path+constructPath(filename)
        html = '''<html>
                  <head><title>Listing</title>
                  <style>
                  
                  </style>
                  </head>
                  <body><a href = '..'>Previous Folder</a><br><hr>
                  <table border=0>'''
        if(os.path.isfile(path)):
            if(os.path.split(path)[-1][0]=='.' and show_hidden==False): #Allow accessing hidden files?
                return "404! Not found."
            return static_file(constructPath(filename), root=serve_path)  #serve a file
        else:
            try:
                os.chdir(path)
                for x in glob.glob('*'):
                    if x==os.path.split(__file__)[-1] or (x[0]=='.' and show_hidden==False):  #Show hidden files?
                        continue
                    scheme = bottle.request.urlparts[0]	#get the scheme of the requested url
                    host = bottle.request.urlparts[1]	#get the hostname of the requested url
                    if i==0:    #alternate rows are colored
                        i+=1
                    else:
                        i=0
                    #just html formatting :D
                    html = html+colorinfo[i]+"<td><a href = '"+scheme+"://"+host+"/"+filename+"/"+x+"'>"+x+"</a><td></tr>"
            except Exception as e:  #Actually an error accessing the file or switching to the directory
                html = "404! Not found."

        return html+"</table><hr><br><br></body></html>" #Append the remaining html code
        

if __name__=='__main__':  #if module is run as a standalone app
    DirLister() #Init the class; starts the app.
